<?php
include_once 'dbMySql.php';
$con = new DB_con();

// data insert code starts here.
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['add-product']))
{
	$SKU = isset($_POST['SKU']) ? stripslashes($_POST['SKU']) : '';
	$Name = isset($_POST['Name']) ? stripslashes($_POST['Name']) : '';
	$Price = isset($_POST['Price']) ? stripslashes($_POST['Price']) : '';
	$Type = isset($_POST['Type']) ? stripslashes($_POST['Type']) : '';
	$Size = isset($_POST['Size']) ? stripslashes($_POST['Size']) : '';
	$Height = isset($_POST['Height']) ? stripslashes($_POST['Height']) : '';
	$Width = isset($_POST['Width']) ? stripslashes($_POST['Width']) : '';
	$Length = isset($_POST['Length']) ? stripslashes($_POST['Length']) : '';
	$Weight = isset($_POST['Weight']) ? stripslashes($_POST['Weight']) : '';
	
	if (empty($SKU)) {
        $errors = 'Please enter required data.';
    }
    if (empty($Name)) {
        $errors = 'Please enter required data.';
    }
    if (empty($Price)) {
        $errors = 'Please enter required data.';
    }
    if (empty($Type)) {
        $errors = 'Please enter required data.';
    }
    if (!empty($Type) && $Type == 'DVD' && empty($Size)) {
        $errors = 'Please enter required data.';
    }

    if (!empty($Type) && $Type == 'Furniture' && empty($Height)) {
        $errors = 'Please enter required data.';
    }
    if (!empty($Type) && $Type == 'Furniture' && empty($Width)) {
        $errors = 'Please enter required data.';
    }
    if (!empty($Type) && $Type == 'Furniture' && empty($Length)) {
        $errors = 'Please enter required data.';
    }
    if (!empty($Type) && $Type == 'Book' && empty($Weight)) {
        $errors = 'Please enter required data.';
    }

    if (!empty($errors)) {
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {

		$result = $con->insert($Name, $SKU, $Price, $Type, $Size, $Height, $Width, $Length, $Weight);
		if($result)
		{
			$data['success'] = true;
        	// $data['message'] = 'Congratulations. Your message has been sent successfully';
		}
		else
		{
			$data['success'] = false;
        	$data['message'] = 'Something went wrong! please try again.';
		}
	}
	echo json_encode($data);
}
// data insert code ends here.

// delete product code starts here.
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['delete-product']))
{
    $product = isset($_POST['product']) ? $_POST['product'] : '';
    
    if (empty($product)) {
        $errors = 'Please select products.';
    }

    if (!empty($errors)) {
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {

        $result = $con->delete($product);
        if($result)
        {
            $data['success'] = true;
            // $data['message'] = 'Congratulations. Your message has been sent successfully';
        }
        else
        {
            $data['success'] = false;
            $data['message'] = 'Something went wrong! please try again.';
        }
    }
    echo json_encode($data);
}
// delete product code ends here.

?>