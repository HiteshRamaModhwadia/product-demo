<?php
define('DB_SERVER','localhost');
define('DB_USER','root');
define('DB_PASS' ,'');
define('DB_NAME', 'practical');
class DB_con
{
	function __construct()
	{
		$con = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
		$this->dbh=$con;
		// Check connection
		if ($con->connect_error) {
		  die("Connection failed: " . $con->connect_error);
		}
	}
	public function insert($Name, $SKU, $Price, $Type, $Size, $Height, $Width, $Length, $Weight)
	{
		$stmt = $this->dbh->prepare("INSERT INTO products (Name, SKU, Price, Type, Size, Height, Width, Length, Weight) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param("sssssssss", $Name, $SKU, $Price, $Type, $Size, $Height, $Width, $Length, $Weight);
		return $stmt->execute();
	}

	public function select()
	{
		return $this->dbh->query('SELECT * FROM products ORDER BY ProductID DESC');
	}

	public function selectSKU($SKU = '')
	{
		$stmt = $this->dbh->prepare("SELECT * FROM products WHERE SKU = ?");
	   	$stmt->bind_param("s", $SKU); 
	   	$stmt->execute();
	   	$result = $stmt->get_result() ;
	   	return $result->num_rows;
	}

	public function delete($data = array())
	{
		// creates a string containing ?,?,? 
		$bindClause = implode(',', array_fill(0, count($data), '?'));
		//create a string for the bind param just containing the right amount of iii
		$bindString = str_repeat('i', count($data));

		$stmt = $this->dbh->prepare('DELETE FROM products WHERE ProductID IN (' . $bindClause . ')');

		$stmt->bind_param($bindString, ...$data);
		return $stmt->execute();

	}
}
?>