<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Add Product</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <style>
            .product{
				border: 1px Solid;
				margin-bottom: 10px;
		    }
            .headerbtn{
				margin-top: 20px;
				margin-bottom: 10px;
				margin-left: 10px;
            }
            .error {
			    color: red;
			    font-size: 14px;
			   	font-weight: normal;
			}
		</style>
	</head>
    <body>
    	<div class="container">
    		<form id="addProduct" action="inc/product.php" method="post">
				<div class="row">
					<div class="col-md-12">
						<h2 class="pull-left">Product Add</h2>
						<a href="index.php" class="btn btn-danger pull-right headerbtn">Cancel</a>
						<button type="submit" name="add-product" class="btn btn-warning pull-right headerbtn">ADD</button>
					</div>
					<div class="col-md-12">
						<hr>
					</div>
				</div>      
				<div class="row">
					<div class="col-md-12 errormsg">
						<div class="form-group">
							<div class="row">
								<div class="col-md-1">
									<label for="sku">SKU</label>
								</div>              
								<div class="col-md-6">
									<input type="text" class="form-control" id="sku" placeholder="Enter SKU" name="SKU">
								</div>
							</div>
						</div>
						<div class="form-group">  
							<div class="row">
								<div class="col-md-1">                          
									<label for="Name">Name</label>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control" id="Name" placeholder="Enter Name" name="Name">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">     
								<div class="col-md-1">                       
									<label for="Price">Price ($)</label>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control number" id="Price" placeholder="Enter Price in $" name="Price">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">     
								<div class="col-md-1">                       
									<label for="Price">Select Type</label>
								</div>
								<div class="col-md-6">
									<select name="Type" id="type" class="form-control">
										<option value="">Select Type</option>
										<option>DVD</option>
										<option>Book</option>
										<option>Furniture</option>
									</select>
								</div>
							</div>
						</div>
						<div class="dvdAttributes" style="display: none">
							<div class="form-group">
								<div class="row">     
									<div class="col-md-1">                       
										<label for="Size">Size (MB)</label>
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control number" id="Size" placeholder="Enter Size in MB" name="Size">
									</div>
								</div>
							</div>
						</div>
						<div class="FurnitureAttributes" style="display: none">
							<div class="form-group">
								<div class="row">     
									<div class="col-md-1">                       
										<label for="Height">Height (CM)</label>
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control number" id="Height" placeholder="Enter Height in CM" name="Height">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">     
									<div class="col-md-1">                       
										<label for="width">width (CM)</label>
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control number" id="width" placeholder="Enter width in CM" name="Width">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">     
									<div class="col-md-1">                       
										<label for="Length">Length (CM)</label>
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control number" id="Length" placeholder="Enter Length in CM" name="Length">
									</div>
								</div>
							</div>
						</div>
						<div class="BookAttributes" style="display: none">		
							<div class="form-group">
								<div class="row">     
									<div class="col-md-1">                       
										<label for="Weight">Weight (KG)</label>
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control number" id="Weight" placeholder="Enter Weight in KG" name="Weight">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<hr>
					</div>
					<div class="col-md-12 text-center">
						<p>Scondiweb Test assignment</h2>
					</div>
				</div> 
			</form>
		</div>
		<!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
        <script>
        	$(document).ready(function(){
        		// On type change dynamic attribute fields
        		$("#type").change(function () {
        			var type = $(this).val();
        			if(type != '')
        			{
        				if(type == 'DVD')
        				{
        					$(".dvdAttributes").show();	
        					$(".BookAttributes").hide();	
        					$(".FurnitureAttributes").hide();	
						}
        				else if(type == 'Book')
        				{
        					$(".dvdAttributes").hide();	
        					$(".BookAttributes").show();	
        					$(".FurnitureAttributes").hide();	
						}
        				else if(type == 'Furniture')
        				{
        					$(".dvdAttributes").hide();	
        					$(".BookAttributes").hide();	
        					$(".FurnitureAttributes").show();
						}
					}
				});
        		
        		(function($) {
					$.fn.inputFilter = function(inputFilter) {
						return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
							if (inputFilter(this.value)) {
								this.oldValue = this.value;
								this.oldSelectionStart = this.selectionStart;
								this.oldSelectionEnd = this.selectionEnd;
								} else if (this.hasOwnProperty("oldValue")) {
								this.value = this.oldValue;
								this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
								} else {
								this.value = "";
							}
						});
					};
				}(jQuery));

        		$(".number").inputFilter(function(value) {
		  		return /^-?[0-9.]*$/i.test(value); });
				
				$.validator.addMethod("noHTML", function(value, element) {
					return this.optional(element) || /^([A-Za-z0-9\n,.'"!@#$%^&*/()+-:;?€ ]+)$/.test(value);
				}, "No HTML tags are allowed!");

				$('#addProduct').validate({
			        ignore:[],
			        rules: {
			            SKU: {
			                required: true,
			                remote:{
								type: "POST",
								url: "inc/ValidateSKU.php",
							},
							noHTML: true,
			            },
			            Name: {
			                required: true,
			            	noHTML: true,
						},
			            Price: {
			                required: true,
			            	noHTML: true,
						},
			            Type: {
			                required: true,
			            },
			            Size: {
			                required: function() {
				                return $('#type').val() == 'DVD'
				            },
							noHTML: true,	              
			            },
			            Height: {
			                required: function() {
				                return $('#type').val() == 'Furniture'
				            },
							noHTML: true,
			            },
			            Width: {
			                required: function() {
				                return $('#type').val() == 'Furniture'
				            },
							noHTML: true,
			            },
			            Length: {
			                required: function() {
				                return $('#type').val() == 'Furniture'
				            },
							noHTML: true,
			            },
			            Weight: {
			                required: function() {
				                return $('#type').val() == 'Book'
				            },
							noHTML: true,
			            },
			        },
			        messages: {
			            SKU: {
			                required: "Please enter sku.",
			                remote: "This sku already added. please enter new sku."
			            },
			            Name: {
			                required: "Please enter name.",
			            },
			            Price: {
			                required: "Please enter price.",
			            },
			            Type: {
			                required: "Please select type.",
			            },
			            Size: {
			                required: "Please enter size.",
			            },
			            Height: {
			                required: "Please enter height.",
			            },
			            Width: {
			                required: "Please enter width.",
			            },
			            Length: {
			                required: "Please enter length.",
							
			            },
			            Weight: {
			                required: "Please enter weight.",
			            },
			        },
			        submitHandler: function(form){
			            try{
			                var $form = $(form);
			                $.ajax({
			                    url:$(form).attr("action"),
			                    type:'POST',
			                    data:$(form).serialize(),
			                    datatype : "application/json",
			                    beforeSend:function(){
			                        $(form).find('button[type="submit"]').prop('disabled', true);
			                    },
			                    success:function(data){
			                        data=JSON.parse(data);
			                        if(data != '')
			                        {
			                        	console.log(data.errors);
			                            if (!data.success) {
			                                if (data.errors != '') {
			                                    $('.errormsg').after('<label class="error">Please enter required data</label>');
			                                }
			                            } else {
			                                $(form).trigger("reset");
			                                window.location.href = 'index.php';
			                            }
			                        }
			                        $(form).find('button[type="submit"]').prop('disabled', false);
			                    },
			                    error: function (jqXHR, exception) {
			                    	console.log(exception);
			                    },
			                });
			            }
			            catch(e)
			            {
			                console.log(e);
			            }
			            return false;
			        },
			        errorPlacement: function(error, element) {
			            error.insertAfter(element);
			        },  
			        invalidHandler: function(form, validator) {
			            var errors = validator.numberOfInvalids();
			            if (errors) {                    
			                validator.errorList[0].element.focus();
			            }
			        } 
			    });
			});

		</script>
	</body>
</html>