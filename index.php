<?php
    include_once 'inc/dbMySql.php';
    $con = new DB_con();
    $result = $con->select();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Product List</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <style>
            .product{
                border: 1px Solid;
                margin-bottom: 10px;
            }
            .headerbtn{
                margin-top: 20px;
                margin-bottom: 10px;
                margin-left: 10px;
            }
            .error {
                color: red;
                font-size: 14px;
                font-weight: normal;
            }
        </style>
    </head>
    <body>
        
        <div class="container">
            <form id="deleteProduct" action="inc/product.php" method="post">
            <div class="row deleteMsg">
                <div class="col-md-12">
                    <h2 class="pull-left">Product List</h2>
                    <button type="submit" name="delete-product" class="delete-checkbox">MASS DELETE</button>
                    <a href="add.php" class="btn btn-warning pull-right headerbtn">ADD</a>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
            </div>                    
            <div class="row">
                <?php
                    foreach ($result as $row) {
                    ?>
                    <div class="col-md-3">
                        <div class="col-md-12 product text-center">
                            <div> <input class="pull-left" type="checkbox" name="product[]" value="<?php echo $row['ProductID']; ?>"> <br><?php echo $row['Name']; ?></div>
                            <div><?php echo $row['SKU']; ?></div>
                            <div><?php echo number_format($row['Price'],2); ?> $</div>
                            <div>
                            <?php
                                if($row['Type'] == 'DVD')
                                {
                                    echo "Size : ".$row['Size'].' MB' ;
                                }
                                else if($row['Type'] == 'Book')
                                {
                                    echo "Weight : ".$row['Weight'].' KG' ;
                                }
                                else if($row['Type'] == 'Furniture')
                                {
                                    echo "Dimension : ".$row['Height'].'X'.$row['Width'].'X'.$row['Length'] ;                                    
                                }
                            ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                ?>
            </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-md-12 text-center">
                    <p>Scondiweb Test assignment</h2>
                </div>
                
            </div> 
        </div>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
        <script>
            $(document).ready(function(){
                $('#deleteProduct').validate({
                    ignore:[],
                    rules: {
                        'product[]': {
                            required: true,
                        },
                    },
                    messages: {
                        'product[]': {
                            required: "Please select product to delete.",
                        },
                    },
                    submitHandler: function(form){
                        try{
                            var $form = $(form);
                            $.ajax({
                                url:$(form).attr("action"),
                                type:'POST',
                                data:$(form).serialize(),
                                datatype : "application/json",
                                beforeSend:function(){
                                    $(form).find('button[type="submit"]').prop('disabled', true);
                                },
                                success:function(data){
                                    data=JSON.parse(data);
                                    if(data != '')
                                    {
                                        console.log(data.errors);
                                        if (!data.success) {
                                            if (data.errors != '') {
                                                $('.deleteMsg').after('<label class="error">Please select products.</label>');
                                            }
                                        } else {
                                            $(form).trigger("reset");
                                            window.location.href = 'index.php';
                                        }
                                    }
                                    $(form).find('button[type="submit"]').prop('disabled', false);
                                },
                                error: function (jqXHR, exception) {
                                    console.log(exception);
                                },
                            });
                        }
                        catch(e)
                        {
                            console.log(e);
                        }
                        return false;
                    },
                    errorPlacement: function(error, element) {
                        error.insertAfter($('.deleteMsg'));
                    },  
                    invalidHandler: function(form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {                    
                            validator.errorList[0].element.focus();
                        }
                    } 
                });
            });

        </script>
    </body>
</html>